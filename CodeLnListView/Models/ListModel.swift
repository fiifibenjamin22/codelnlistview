//
//  ListModel.swift
//  CodeLnListView
//
//  Created by Benjamin Acquah on 20/09/2019.
//  Copyright © 2019 Benjamin Acquah. All rights reserved.
//

import UIKit

class ListDataModel {
    
    var albumId = 0
    var id = 0
    var title = ""
    var placeholderUrl = ""
    var artImage = ""
    
}
