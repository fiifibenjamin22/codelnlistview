//
//  DetailView.swift
//  CodeLnListView
//
//  Created by Benjamin Acquah on 20/09/2019.
//  Copyright © 2019 Benjamin Acquah. All rights reserved.
//

import UIKit
import AlamofireImage

//Detail View

class DetailView: UIViewController,UISearchBarDelegate {
    
    lazy var searchBar = UISearchBar(frame: .zero)
    var img = ""
    var titleData = ""
    
    //UIComponents
    let ImageAvatar: UIImageView = {
        let img = UIImageView()
        img.layer.borderWidth = 1
        img.backgroundColor = .white
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //refactor
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Title 1"
        lbl.numberOfLines = 2
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        return lbl
    }()
    
    let contentLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Lorem ipsum is simple"
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textColor = .gray
        return lbl
    }()
    
    func baseButton(title:String) -> UIButton {
        let button = UIButton(type: .system)
        let color = UIColor.darkText
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.setTitleColor(color, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    func baseButtonWithImage(title:String, tinting: UIColor) -> UIButton {
        let button = UIButton(type: .system)
        let color = UIColor.darkText
        let img = UIImage(named: title)?.withRenderingMode(.alwaysTemplate)
        button.tintColor = .white
        button.backgroundColor = tinting
        button.setImage(img, for: .normal)
        button.layer.cornerRadius = 0
        button.layer.borderWidth = 1
        button.setTitleColor(color, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    lazy var goBtn: UIButton = {
        let btn = baseButton(title: "Go")
        btn.backgroundColor = UIColor.magenta
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    lazy var callBtn: UIButton = {
        let btn = baseButtonWithImage(title: "call", tinting: .black)
        return btn
    }()
    
    lazy var emailBtn: UIButton = {
        let btn = baseButtonWithImage(title: "message",tinting: .blue)
        return btn
    }()
    
    let moreContentLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.numberOfLines = 10
        lbl.textColor = .gray
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.searchSetup()
        self.setupView()
    }
    
    func searchSetup(){
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
        searchBar.delegate = self
    }
    
    func setupView(){
        self.view.addSubview(ImageAvatar)
        self.view.addSubview(goBtn)
        self.view.addSubview(titleLbl)
        self.view.addSubview(contentLbl)
        self.view.addSubview(moreContentLbl)
        self.view.addSubview(callBtn)
        self.view.addSubview(emailBtn)
        
        self.ImageAvatar.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.ImageAvatar.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.ImageAvatar.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.ImageAvatar.heightAnchor.constraint(equalToConstant: 350).isActive = true
        
        self.goBtn.topAnchor.constraint(equalTo: ImageAvatar.bottomAnchor, constant: 16).isActive = true
        self.goBtn.rightAnchor.constraint(equalTo: ImageAvatar.rightAnchor, constant: -8).isActive = true
        self.goBtn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.goBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        titleLbl.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 8).isActive = true
        titleLbl.topAnchor.constraint(equalTo: goBtn.topAnchor).isActive = true
        titleLbl.rightAnchor.constraint(equalTo: goBtn.leftAnchor, constant: -5).isActive = true
        titleLbl.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        contentLbl.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 8).isActive = true
        contentLbl.topAnchor.constraint(equalTo: titleLbl.bottomAnchor, constant: 0).isActive = true
        contentLbl.rightAnchor.constraint(equalTo: goBtn.leftAnchor, constant: -5).isActive = true
        contentLbl.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        
        moreContentLbl.topAnchor.constraint(equalTo: contentLbl.bottomAnchor, constant: 8).isActive = true
        moreContentLbl.leftAnchor.constraint(equalTo: titleLbl.leftAnchor).isActive = true
        moreContentLbl.rightAnchor.constraint(equalTo: goBtn.rightAnchor).isActive = true
        moreContentLbl.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -70).isActive = true
        
        callBtn.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        callBtn.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        callBtn.widthAnchor.constraint(equalToConstant: view.frame.width / 2).isActive = true
        callBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        emailBtn.leftAnchor.constraint(equalTo: callBtn.rightAnchor).isActive = true
        emailBtn.bottomAnchor.constraint(equalTo: callBtn.bottomAnchor).isActive = true
        emailBtn.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        emailBtn.topAnchor.constraint(equalTo: callBtn.topAnchor).isActive = true
        
        self.setData()
    }
    
    func setData(){
        
        if  !((img.isEmpty)) {
            self.ImageAvatar.af_setImage(
                withURL: URL(string: (img))!,
                placeholderImage: UIImage(named: "placeholder"),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.ImageAvatar.image = UIImage(named: "placeholder")
        }
        
        self.titleLbl.text = "\(titleData)"
    }
}

