//
//  MainListViewCell.swift
//  CodeLnListView
//
//  Created by Benjamin Acquah on 20/09/2019.
//  Copyright © 2019 Benjamin Acquah. All rights reserved.
//

import UIKit
import AlamofireImage

//Cell for the collection view laidout for list
class MainListViewCell: UICollectionViewCell {
    
    var listObject: ListDataModel? {
        didSet {
            guard let unwrapedItem = listObject else { return }
            if  !((unwrapedItem.artImage.isEmpty)) {
                self.ImageAvatar.af_setImage(
                    withURL: URL(string: (unwrapedItem.artImage))!,
                    placeholderImage: UIImage(named: "placeholder"),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.ImageAvatar.image = UIImage(named: "placeholder")
            }
            self.titleLbl.text = "\(unwrapedItem.title)"
            self.contentLbl.text = "Lorem ipsum is simple"
        }
    }
    //UIComponents
    let ImageAvatar: UIImageView = {
        let img = UIImageView()
        img.layer.borderWidth = 1
        img.backgroundColor = .white
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //refactor
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Title 1"
        lbl.numberOfLines = 2
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        return lbl
    }()
    
    let contentLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "content text goes here"
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textColor = .gray
        return lbl
    }()
    
    let seperator: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    func setupViews() {
        
        self.addSubview(ImageAvatar)
        self.addSubview(titleLbl)
        self.addSubview(contentLbl)
        self.addSubview(seperator)
        
        ImageAvatar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        ImageAvatar.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        ImageAvatar.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        ImageAvatar.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        
        titleLbl.leftAnchor.constraint(equalTo: ImageAvatar.rightAnchor, constant: 8).isActive = true
        titleLbl.topAnchor.constraint(equalTo: ImageAvatar.topAnchor).isActive = true
        titleLbl.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        titleLbl.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        contentLbl.topAnchor.constraint(equalTo: titleLbl.bottomAnchor, constant: 8).isActive = true
        contentLbl.leftAnchor.constraint(equalTo: titleLbl.leftAnchor).isActive = true
        contentLbl.rightAnchor.constraint(equalTo: titleLbl.rightAnchor).isActive = true
        contentLbl.bottomAnchor.constraint(equalTo: ImageAvatar.bottomAnchor).isActive = true
        
        seperator.topAnchor.constraint(equalTo: ImageAvatar.bottomAnchor, constant: 3).isActive = true
        seperator.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        seperator.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        seperator.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
