//
//  ViewController.swift
//  CodeLnListView
//
//  Created by Benjamin Acquah on 20/09/2019.
//  Copyright © 2019 Benjamin Acquah. All rights reserved.
//

import UIKit

class MainListViewCV: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    let cellId = "CellID"
    var listData = [ListDataModel]()
    lazy var searchBar = UISearchBar(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchCall()
        self.collectionView.backgroundColor = .white
        self.collectionView.register(MainListViewCell.self, forCellWithReuseIdentifier: cellId)
        self.navItem()
        self.searchSetup()
    }
    
    func navItem(){
        let img = UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal)
        let Menu = UIBarButtonItem(image: img, style: .plain, target: self, action: #selector(MenuTapped))
        navigationItem.leftBarButtonItems = [Menu]
    }
    
    func searchSetup(){
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
        searchBar.delegate = self
    }
    
    @objc func MenuTapped(){
        
    }
    
    func fetchCall() {
        ApiService().getList { (status, dataIn, message) in
            if status == ApiCallStatus.SUCCESS {
                self.listData.append(contentsOf: dataIn!)
                self.collectionView.reloadData()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MainListViewCell
        let items = listData[indexPath.row]
        cell.listObject = items
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailView()
        vc.img = listData[indexPath.item].artImage
        vc.titleData = listData[indexPath.item].title
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
