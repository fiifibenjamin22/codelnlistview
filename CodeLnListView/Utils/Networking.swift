//
//  Networking.swift
//  CodeLnListView
//
//  Created by Benjamin Acquah on 20/09/2019.
//  Copyright © 2019 Benjamin Acquah. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiService {
    
    let url = "https://jsonplaceholder.typicode.com/photos"
    
    func getList(completion: @escaping(ApiCallStatus,[ListDataModel]?,String) -> ()) {
        
        Alamofire.request(url, method: .get).responseJSON { response in
            
            //check response status
            if let status = response.response?.statusCode {
                switch(status){
                case 200...300:
                    var listArray = [ListDataModel]()
                    let item = try? JSON(data: response.data!)
                    //let theList = self.parseListData(item: item!)
                    
                    for itm in (item?.enumerated())! {
                        let theList = self.parseListData(item: itm.element.1)
                        //print(itm.element.1)
                        //listArray.removeAll()
                        listArray.append(theList)
                        //print(listArray.count)
                    }
                    
                    completion(.SUCCESS,listArray,"Data Retrieved Succussfully")
                    //self.parseListData(item: response.data)
                case 301...499:
                    print("something wrong")
                default:
                    completion(.FAILED,nil,"Your request failed, try again later.")
                }
                
                
            }
            
        }
    }
    
    func parseListData(item: JSON) -> ListDataModel {
        //print(item)
        let listDataObject = ListDataModel()
        listDataObject.albumId = item["albumId"].intValue
        listDataObject.id = item[" id"].intValue
        listDataObject.artImage = item["thumbnailUrl"].stringValue
        listDataObject.title = item["title"].stringValue
        listDataObject.placeholderUrl = item["url"].stringValue
        //print(listDataObject.title)
        return listDataObject
    }
}

enum ApiCallStatus {
    case FAILED
    case SUCCESS
    case WARNING
    case DETAIL
}
